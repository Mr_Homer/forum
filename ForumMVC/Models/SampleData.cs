﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ForumMVC.Models
{
    public class SampleData: DropCreateDatabaseIfModelChanges<ForumDB>
    {
        protected override void Seed(ForumDB context)
        {
            var maintopic = new List<MainTopic>
            {
                new MainTopic{Name="News"},
                new MainTopic{Name="History"},
                new MainTopic{Name="Programing"}
            };

            new List<SecondTopic>
            {
                new SecondTopic{Title="Macedonia", MainTopic=maintopic.Single(m=>m.Name=="News")},
                new SecondTopic{Title="Economics", MainTopic=maintopic.Single(m=>m.Name=="News")},
                new SecondTopic{Title="C#", MainTopic=maintopic.Single(m=>m.Name=="Programing")},
                new SecondTopic{Title=".NET MVC3", MainTopic=maintopic.Single(m=>m.Name=="Programing")},
                new SecondTopic{Title="Pyramids", MainTopic=maintopic.Single(m=>m.Name=="History")}
            }.ForEach(m => context.SecondTopics.Add(m));
        }
    }
}