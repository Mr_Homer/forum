﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ForumMVC.Models
{
    [Bind(Exclude="SecondTopicId")]
    public class SecondTopic
    {
        [ScaffoldColumn(false)]
        public int SecondTopicId { get; set; }
        [DisplayName("MainTopic")]
        public int MainTopicId { get; set; }
        [Required(ErrorMessage="An SecondTopic Title is required")]
        [StringLength(160)]
        public string Title { get; set; }
        public System.DateTime DateCreated { get; set; }

        public virtual MainTopic MainTopic { get; set; }
    }
}