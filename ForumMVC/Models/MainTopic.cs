﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForumMVC.Models
{
    public class MainTopic
    {
        public int MainTopicId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<SecondTopic> SecondTopics { get; set; }

    }
}