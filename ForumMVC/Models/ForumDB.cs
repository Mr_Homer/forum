﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ForumMVC.Models
{
    public class ForumDB :DbContext
    {
        public DbSet<MainTopic> MainTopics { get; set; }
        public DbSet<SecondTopic> SecondTopics { get; set; }
    }
}