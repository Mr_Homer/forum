﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumMVC.Models;

namespace ForumMVC.Controllers
{ 
    [Authorize(Roles="Administrator")]
    public class ForumManagerController : Controller
    {
        private ForumDB db = new ForumDB();

        //
        // GET: /ForumManager/

        public ViewResult Index()
        {
            var secondtopics = db.SecondTopics.Include(s => s.MainTopic);
            return View(secondtopics.ToList());
        }

        //
        // GET: /ForumManager/Details/5

        public ViewResult Details(int id)
        {
            SecondTopic secondtopic = db.SecondTopics.Find(id);
            return View(secondtopic);
        }

        //
        // GET: /ForumManager/Create

        public ActionResult Create()
        {
            ViewBag.MainTopicId = new SelectList(db.MainTopics, "MainTopicId", "Name");
            return View();
        } 

        //
        // POST: /ForumManager/Create

        [HttpPost]
        public ActionResult Create(SecondTopic secondtopic)
        {
            if (ModelState.IsValid)
            {
                db.SecondTopics.Add(secondtopic);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.MainTopicId = new SelectList(db.MainTopics, "MainTopicId", "Name", secondtopic.MainTopicId);
            return View(secondtopic);
        }
        
        //
        // GET: /ForumManager/Edit/5
 
        public ActionResult Edit(int id)
        {
            SecondTopic secondtopic = db.SecondTopics.Find(id);
            ViewBag.MainTopicId = new SelectList(db.MainTopics, "MainTopicId", "Name", secondtopic.MainTopicId);
            return View(secondtopic);
        }

        //
        // POST: /ForumManager/Edit/5

        [HttpPost]
        public ActionResult Edit(SecondTopic secondtopic)
        {
            if (ModelState.IsValid)
            {
                db.Entry(secondtopic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MainTopicId = new SelectList(db.MainTopics, "MainTopicId", "Name", secondtopic.MainTopicId);
            return View(secondtopic);
        }

        //
        // GET: /ForumManager/Delete/5
 
        public ActionResult Delete(int id)
        {
            SecondTopic secondtopic = db.SecondTopics.Find(id);
            return View(secondtopic);
        }

        //
        // POST: /ForumManager/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            SecondTopic secondtopic = db.SecondTopics.Find(id);
            db.SecondTopics.Remove(secondtopic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}