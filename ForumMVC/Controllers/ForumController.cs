﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumMVC.Models;

namespace ForumMVC.Controllers
{
    public class ForumController : Controller
    {
        //
        // GET: /Forum/

        ForumDB forumDB = new ForumDB();

        public ActionResult Index()
        {
            var mainTopics = forumDB.MainTopics.ToList();
            return View(mainTopics);
        }
        public ActionResult Browse(string mainTopic)
        {
            var mainTopicModel = forumDB.MainTopics.Include("SecondTopics").Single(s => s.Name == mainTopic);
            return View(mainTopicModel);
        }
        public ActionResult Details(int id)
        {
            var secondTopic = forumDB.SecondTopics.Find(id);
            return View(secondTopic);
        }
        [ChildActionOnly]
        public ActionResult MainTopicMenu()
        {
            var maintopics = forumDB.MainTopics.ToList();
            return PartialView(maintopics);
        }
    
    }
}
